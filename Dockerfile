FROM alpine

RUN apk add vnstat --update-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ --allow-untrusted \
	&& apk -U add --no-cache curl bash \
    && curl https://bin.equinox.io/c/ekMN3bCZFUn/forego-stable-linux-amd64.tgz -O \
	&& cd /usr/local/bin \
	&& tar -xf /forego-stable-linux-amd64.tgz \
	&& rm /forego-stable-linux-amd64.tgz \
    && apk del curl

ADD ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

RUN echo "vnstatd: /usr/sbin/vnstatd -n --config /tmp/vnstat.conf --noadd"> /Procfile

VOLUME /var/lib/vnstat

ENTRYPOINT /entrypoint.sh
