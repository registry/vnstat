#!/bin/sh

VNSTAT_IF="${VNSTAT_IF:-eth0}"

if [ ! -f "/tmp/vnstat.conf" ]; then
	cp /etc/vnstat.conf /tmp
	sed -i 's/Interface.*/Interface '"${VNSTAT_IF}"'/' /tmp/vnstat.conf
	vnstat -u -i ${VNSTAT_IF}
fi

exec /usr/local/bin/forego start -f /Procfile

